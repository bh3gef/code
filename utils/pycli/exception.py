class JsonConfInitErrorException(Exception):
    pass

class DeviceAddrClashException(Exception):
    pass

class SKYError(Exception):
    pass

class CMDError(SKYError):
    def __init__(self, value):
        self.message = value
    def __str__(self):
        return repr(self.message)

class DLLError(SKYError):
    def __init__(self, value):
        self.message = value
    def __str__(self):
        return repr(self.message)

import se_path
ERROR_ALL_F = '0xffffffff'
class SkyeyeAPIException(Exception):
    Language = 'ch'
    err_json = None

    def __init__(self, errMsg):
        self._errMsg = errMsg
        self.err_json = None
        self.err_str = None

    def __str__(self):
        return self.msg()

    def msg(self):
        if self.err_str is not None:
            return self.err_str

        if self._errMsg[0] != ERROR_ALL_F:
            if SkyeyeAPIException.err_json is None:
                import json
                try:
                    with open(se_path.ErrorDataPath, encoding='utf-8') as f:
                        SkyeyeAPIException.err_json = json.load(f)
                except Exception as e:
                    raise Exception('读取错误数据文件出错')

            id = int(self._errMsg[0], 0)
            l_id = hex(id & 0xffff)
            h_id = hex(id - (id & 0xffff))
            try:
                msg1 = SkyeyeAPIException.err_json[h_id][SkyeyeAPIException.Language]
                msg2 = SkyeyeAPIException.err_json[l_id][SkyeyeAPIException.Language]
            except Exception as e:
                raise Exception("未找到该ID信息, ID: %x" % id)
            k = 0
            for n in range(5):
                k = msg2.find('%s', k)
                if k == -1:
                    break
                k += 2

            if len(self._errMsg) - 1 >= n:
                msg2 %= tuple(self._errMsg[1:1+n])
            self.err_str = msg1 + ' : ' + msg2
        else:
            self.err_str = self._errMsg[-1]
        return self.err_str
