import conf2
from threading import Timer
from skyeye_common_module import SkyEyeGetCpuSteps
from collections import OrderedDict

class MIPS:
    _instance = None

    @staticmethod
    def instance():
        if MIPS._instance is None:
            MIPS._instance = MIPS()
        return MIPS._instance

    def __init__(self):
        conf2.add_config_done_callback('mips_start', self.start)
        conf2.add_config_clear_callback('mips_stop', self.stop)

    def start(self):
        self.running = True 
        Timer(1.0, self.update).start()
        self.config = conf2.get_config()
        self.cpus = self.config.get_cpus()
        self.cpu_steps = OrderedDict()
        self.cpu_mips = OrderedDict()
        for cpu in self.cpus:
            self.cpu_steps[cpu] = 0
            self.cpu_mips[cpu] = 0

    def stop(self):
        self.running = False

    def update(self):
        if self.running:
            Timer(1.0, self.update).start()
            for cpu in self.cpus:
                steps = SkyEyeGetCpuSteps(cpu)
                self.cpu_mips[cpu] = steps - self.cpu_steps[cpu]
                self.cpu_steps[cpu] = steps

    def get_mips(self):
        return self.cpu_mips

# API
def init():
    MIPS.instance()

def get():
    obj = MIPS.instance()
    return obj.get_mips()
