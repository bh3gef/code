from exception import SkyeyeAPIException, ERROR_ALL_F
from skyeye_common_module import *
from collections import OrderedDict

class Node:
    def __init__(self, name=None, parent=None, **kwargs):
        self.name = name
        self.parent = parent
        for k, v in kwargs.items():
            setattr(self, k, v)
        self.cli_attrs = {}
        self.object_cmd, self.object_cmd_kwargs = self.create_obj_command()
        self.attach_to_parent(parent)

    def attach_to_parent(self, parent):
        if parent:
            parent.add_child(self.name, self)

    def add_child(self, name, child):
        self.cli_attrs[name] = child
        self.add_child_callback(child)

    # Implement this function if need
    def add_child_callback(self, child):
        pass

    def find_child(self, name):
        return self.cli_attrs.get(name, None)

    def find_obj_by_path(self, path):
        if path:
            if '.' in path:
                obj_name, path = path.split('.', 1)
            else:
                obj_name, path = path, ''

            if obj_name in self.cli_attrs:
                obj = self.cli_attrs[obj_name].find_obj_by_path(path)
            else:
                obj = None
        else:
            obj = self

        return obj

    def match_cli_attr_name(self, text):
        return [name for name in self.cli_attrs if name.startswith(text)]

    def create_obj_command(self):
        from skyeye_command._object_command import ObjectCommand
        if self.name:
            cmd = ObjectCommand(self.name)
        else:
            cmd = None
        cmd_kwargs = {'obj': self}
        return cmd, cmd_kwargs

    def __call__(self, arg=''):
        if self.object_cmd:
            self.object_cmd(arg=arg, **self.object_cmd_kwargs)

class Root(Node):
    def __str__(self):
        return '<Root>'

class MethodNode(Node):
    def __init__(self, name, parent, cmd):
        super().__init__(name, parent, cmd=cmd)

    def create_obj_command(self):
        return self.cmd, {'obj': self.parent}

class Component(Node):
    def __init__(self, name, parent, base='', cls='', iface_list=(), attr_list=()):
        super().__init__(name, parent, base=base, cls=cls, \
                        iface_list=iface_list, attr_list=attr_list)
        self.instance()
        self.register_method_commands()

    def instance(self):
        raise NotImplementedError

    def register_method_commands(self):
        pass

    def connect(self):
        for iface_name, slots in self.iface_list:
            if 'iface' in slots:
                slots = [slots]

            if self.cls == 'memory_space' and iface_name == 'memory_space':
                for index, slot in enumerate(slots):
                    j = slot.index('iface')
                    target = slot[j+1]
                    target_obj = self.parent.find_child(target)
                    if isinstance(slot[j+2], list):
                        for addr, length in slot[j+2]:
                            addr = int(addr, 0)
                            length = int(length, 0)
                            SkyEyeMpAddMapGroup(self.name, target, addr, length - 1, index)
                            target_obj.memory_space_map(self.name, addr, length)
                    else:
                        addr, length = slot[j+2:]
                        addr = int(addr, 0)
                        length = int(length, 0)
                        SkyEyeMpAddMap(self.name, target, addr, length - 1)
                        target_obj.memory_space_map(self.name, addr, length)
            else:
                for slot in slots:
                    j = slot.index('iface')
                    target = slot[j+1]
                    try:
                        index = int(slot[j+2], 0)
                    except IndexError:
                        index = 0
                    SkyEyeConnect(self.name, target, iface_name, index)

    def set_attr(self):
        for attr_name, (attr_type, value) in self.attr_list:
            SkyEyeSetAttr(self.name, attr_name, attr_type, value)

class Machine(Component):
    def instance(self):
        SkyEyeCreateMach(self.name, self.cls)

    def register_method_commands(self):
        from skyeye_command._object_command import Machine_list_cpu, Machine_list_device
        cmd = Machine_list_cpu('list_cpu')
        MethodNode(name='list_cpu', parent=self, cmd=cmd)

        cmd = Machine_list_device('list_device')
        MethodNode(name='list_device', parent=self, cmd=cmd)

    def __str__(self):
        return '<Machine: "%s">' % self.name

    def get_cpus(self):
        res = OrderedDict()
        for name, child in self.cli_attrs.items():
            if isinstance(child, Cpu):
                res[name] = child
        return res

    def get_devices(self):
        res = OrderedDict()
        for name, child in self.cli_attrs.items():
            if isinstance(child, Device):
                res[name] = child
        return res

class Cpu(Component):
    def instance(self):
        SkyEyeCreateCpu(self.parent.name, self.name, self.cls)

    def __str__(self):
        return '<Cpu: "%s">' % self.name

class Device(Component):
    def instance(self):
        SkyEyeCreateDevice(self.parent.name, self.name, self.cls)

    def __str__(self):
        return '<Device: "%s">' % self.name

    def memory_space_map(self, space_name, addr, length):
        if not hasattr(self, '_space_name'):
            self._space_maps = OrderedDict()
        self._space_maps[addr] = (space_name, length)
        self.create_register_list(addr)

    def has_registers(self):
        num = 0
        try:
            num = SkyEyeGetDevRegNum(None, self.name)
        except:
            pass
        return num > 0

    def create_register_list(self, base_addr):
        if self.has_registers():
            RegisterList('regs', self, self.name, base_addr)

    def get_register_list(self):
        return self.cli_attrs.get('regs', None)

class Linker(Component):
    def instance(self):
        SkyEyeCreateLinker(self.name, self.cls)

    def __str__(self):
        return '<Linker: "%s">' % self.name

class RegisterList(Node):
    def __init__(self, name, parent, device_name, base_addr):
        super().__init__(name, parent, device_name=device_name, base_addr=base_addr)
        self.create_register()

    def create_register(self):
        num = SkyEyeGetDevRegNum(None, self.device_name)
        for reg_id in range(num):
            reg_name = SkyEyeGetDevRegNameById(None, self.device_name, reg_id)
            offset = SkyEyeGetDevRegOffsetById(None, self.device_name, reg_id)
            if offset == 0xffffffff:
                offset = reg_id * 4
            Register(reg_name, self, self.device_name, reg_id, self.base_addr+offset)

    def create_obj_command(self):
        from skyeye_command._object_command import RegisterListCommand
        cmd = RegisterListCommand(str(self))
        return cmd, {'obj': self}

    def get_registers(self):
        return self.cli_attrs.copy()

    def get_register(self, name):
        return self.cli_attrs.get(name, None)

    def __str__(self):
        return '<RegisterList of %s>' % self.device_name

class Register(Node):
    def __init__(self, name, parent, device_name, reg_id, addr):
        super().__init__(name, parent, device_name=device_name, reg_id=reg_id, addr=addr)

    def create_obj_command(self):
        from skyeye_command._object_command import RegisterCommand
        cmd = RegisterCommand(str(self))
        return cmd, {'obj': self}

    @property
    def value(self):
        try:
            value = SkyEyeGetDevRegValueById(None, self.device_name, self.reg_id)
        except:
            return None
        return value

    @value.setter
    def value(self, v):
        pass

    def __str__(self):
        return '<Register of %s: %s>' % (self.device_name, self.name)

    def get_device_name(self):
        return self.device_name

    def get_id(self):
        return self.reg_id

    def get_addr(self):
        return self.addr
