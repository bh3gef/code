from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='list_attr',
                description='Show attribute of a class.',
                add_help=False)

        parser.add_argument(
                'cls', 
                metavar='<class-name>',
                help='class name',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        attr_info_l = []
        cls_l = SkyEyeGetClassList()

        if arg_ns.cls not in cls_l:
            msg = '"%s" ATTR information was not found' % arg_ns.cls
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        attr_l = SkyEyeGetClassAttrList(arg_ns.cls)
        attr_l.sort()
        attr_info_l = []
        for attr in attr_l: 
            info_l = SkyEyeGetClassAttrInfo(arg_ns.cls, attr)
            attr_info_l.append(info_l)

        print ("%-20s%-20s%s" % ("AttrName", "Type", "Description"))
        #BUG len(info_l) == 4
        for info_l in attr_info_l:
            if len(info_l) != 3:
                destription = 'NULL'
            else:
                destription = info_l[2]
            print("%-20s%-20s%s" % (info_l[0], info_l[1], destription))

        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        cls_l = SkyEyeGetClassList()
        return [item for item in cls_l if item.startswith(text)]
