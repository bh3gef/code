from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
import fault_inject as sfi
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='disable_device_work',
                description='disable_device_work',
                add_help=False)

        parser.add_argument(
                'device', 
                metavar='<device-name>',
                help='device name',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        config = conf2.get_config()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        if arg_ns.device not in config.get_devices():
            raise SkyeyeAPIException(['0x40050004', arg_ns.device])
        try:
            SkyEyeDisableDeviceWork(arg_ns.device)
        except Exception as e:
            raise SkyeyeAPIException(['0x40050000', e])

        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        config = conf2.get_config()
        if not config:
            return []
        dl = config.get_devices()
        return [item for item in dl if item.startswith(text)]
