from . import SkyeyeCommand
import argparse
import os
from exception import SkyeyeAPIException, ERROR_ALL_F
#from conf import json_conf, define_json_file, SetGlobalConfig
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='define_conf',
                description='config the machine.',
                add_help=False)

        parser.add_argument(
                'path', 
                metavar='<config-json-path>',
                help='json config file path',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        if script:
            script_dir = os.path.dirname(script)
            path = os.path.join(script_dir, arg_ns.path)
        else:
            path = arg_ns.path
        
        if not os.path.exists(path):
            raise SkyeyeAPIException(['0x401c0002', path])

        conf2.make_config(path)
        if cli:
            cli.update_config()

        if cli:
            cli.open_conf_flag = True

        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        dir_name, last = os.path.split(text)
        top = dir_name if dir_name else '.'
        _, dirs, files, _ = next(os.fwalk(top))
        if last == '.':
            dirs += ['.', '..']
        elif last == '..':
            dirs += ['..']
        dirs = [(os.path.join(dir_name, item))+'/' for item in dirs]
        files = [(os.path.join(dir_name, item)) for item in files 
                            if os.path.splitext(item)[1] == '.json']
        items = files + dirs
        return [item for item in items if item.startswith(text)]
