from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import SkyEyeCreateBreakpoint
from exception import SkyeyeAPIException, ERROR_ALL_F
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='list_cpu',
                description='Show all available cpu.',
                add_help=False)

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        config = conf2.get_config()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        machines = config.get_machines()
        print ("%-30s%-30s" % ("CpuName", "BelongBoard"))
        for mach, mach_obj in machines.items():
            for cpu in mach_obj.get_cpus():
                print  ("%-30s%-30s" % (cpu, mach))

        return False
