from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
import fault_inject as sfi
import mips

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='speed',
                description='Show the current running speed of the cpu.',
                add_help=False)

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        mips_dict = mips.get()
        for item in mips_dict:
            print ("CPU:%20s  SPEED:%d" % (item, mips_dict[item]))

        return False
