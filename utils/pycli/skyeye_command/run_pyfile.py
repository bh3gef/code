from . import SkyeyeCommand
import argparse
from exception import SkyeyeAPIException, ERROR_ALL_F
from skyeye_common_module import *

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='run_pyfile',
                description='run a python script',
                add_help=False)

        parser.add_argument(
                'path', 
                metavar='<python-file>',
                default='',
                help='python file',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        try:
            self._exec_py_file(arg_ns.path)
        except:
            import traceback
            raise SkyeyeAPIException([ERROR_ALL_F, traceback.format_exc()])

        return False

    def _exec_py_file(self, path):
        del self
        with open(path) as f:
            exec(f.read())

    def complete_arg_1(self, text, line, begidx, endidx):
        dir_name, last = os.path.split(text)
        top = dir_name if dir_name else '.'
        _, dirs, files, _ = next(os.fwalk(top))
        if last == '.':
            dirs += ['.', '..']
        elif last == '..':
            dirs += ['..']
        dirs = [(os.path.join(dir_name, item))+'/' for item in dirs]
        files = [(os.path.join(dir_name, item)) for item in files 
                            if os.path.splitext(item)[1] == '.py']
        items = files + dirs
        return [item for item in items if item.startswith(text)]
