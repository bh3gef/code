from . import SkyeyeCommand
import argparse
import os
import skyeye_cli
from exception import SkyeyeAPIException, ERROR_ALL_F

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='help',
                description='Print help info.',
                add_help=False,
                )

        parser.add_argument(
                'cmd', 
                metavar='<cmd>',
                nargs='?', 
                help='command name',
                )

        return parser

    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        arg = arg.replace('-', '_')
        return super(skyeye_cli.SkyEyeCli, cli).do_help(arg)
