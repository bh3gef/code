from . import SkyeyeCommand
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='ls',
                description='list directory contents',
                add_help=False)

        parser.add_argument(
                'path', 
                metavar='<path>',
                nargs='?',
                default='',
                help='the path of file or directory',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        if arg_ns.path and not os.path.exists(arg_ns.path):
            raise SkyeyeAPIException([ERROR_ALL_F, "No such file or directory"])

        SkyEyeListDir(arg)
        return False
