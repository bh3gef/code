from . import SkyeyeCommand
import argparse
import os
import cli
from tools import SkyEyeSetScriptPath
from exception import SkyeyeAPIException, ERROR_ALL_F
import project_config as pc
from importlib import import_module, reload

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='run_script',
                description='Run skyeye script.',
                add_help=False)

        parser.add_argument(
                'command', 
                nargs='+',
                metavar='<one-command>',
                help='one-command',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        name, arg = arg_ns.command[0], ' '.join(arg_ns.command[1:])
        skyeye_command = import_module('skyeye_command')
        command = getattr(skyeye_command, name, None)
        if command is None:
            msg = 'Unknown command: %s' % name
            raise SkyeyeAPIException([ERROR_ALL_F, msg])
        command(arg=arg, cli=cli)

        return False
