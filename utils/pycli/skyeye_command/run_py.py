from . import SkyeyeCommand
import argparse
from exception import SkyeyeAPIException, ERROR_ALL_F
from skyeye_common_module import *

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='run_py',
                description='run python expressions',
                add_help=False)

        parser.add_argument(
                'expr', 
                metavar='<python-expr>',
                nargs='*',
                default='',
                help='python expressions',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        try:
            self._run_python_expr(arg)
        except:
            import traceback
            raise SkyeyeAPIException([ERROR_ALL_F, traceback.format_exc()])

        return False

    def _run_python_expr(self, _expr):
        del self
        exec(_expr)
