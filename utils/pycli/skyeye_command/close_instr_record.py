from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
from conf import GetGlobalConfig
import fault_inject as sfi
from conf import *

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='close_instr_record',
                description='close_instr_record',
                add_help=False)

        parser.add_argument(
                'cpu', 
                metavar='<cpu-name>',
                help='cpu name',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        config = GetGlobalConfig()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        if arg_ns.cpu not in config.get_cpu_list():
            raise SkyeyeAPIException(['0x401f0004', arg_ns.cpu])

        try:
            ret = SkyEyeCloseInstrRecord(arg_ns.cpu)
        except Exception as e:
            raise SkyeyeAPIException(['0x401f0000', e])
        if ret == 0:
            raise SkyeyeAPIException(['0x401f1f01', e])

        return False
