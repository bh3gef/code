from . import SkyeyeCommand
import argparse
import os
import skyeye_cli
from exception import SkyeyeAPIException, ERROR_ALL_F

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='history',
                description='Show command history.',
                add_help=False)

        return parser

    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        hist_len = cli.readline.get_current_history_length()
        for i in range(1, hist_len+1):
            hist_item = cli.readline.get_history_item(i)
            print('%-5s %s' % (i, hist_item))
        
        return False
