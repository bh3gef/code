from . import SkyeyeCommand
import argparse
import os
from exception import SkyeyeAPIException, ERROR_ALL_F

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='cd',
                description='Change the Skyeye working directory.',
                add_help=False)

        parser.add_argument(
                'path', 
                metavar='<path>',
                help='the path of directory',
                )
        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        os.chdir(arg_ns.path)
        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        dir_name, last = os.path.split(text)
        top = dir_name if dir_name else '.'
        _, dirs, _, _ = next(os.fwalk(top))
        if last == '.':
            dirs += ['.', '..']
        elif last == '..':
            dirs += ['..']
        dirs = [(os.path.join(dir_name, item))+'/' for item in dirs]
        return [item for item in dirs if item.startswith(text)]
