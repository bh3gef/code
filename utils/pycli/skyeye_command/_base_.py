from abc import ABC, abstractmethod
from exception import SkyeyeAPIException, ERROR_ALL_F
from state_machine.skyeye_state_machine import state_machine

class SkyeyeCommand(ABC):
    export = []
    need_check_arg = True
    need_check_state = True

    def __init__(self, name=None):
        self.name = name
        self.argparser = self.create_argparser()

    def print_help(self):
        self.argparser.print_help()

    @abstractmethod
    def call(self, *args, **kwargs):
        pass

    @abstractmethod
    def create_argparser(self):
        pass

    def complete(self, text, line, begidx, endidx):
        i = len(line.split())
        if begidx < endidx:
            i -= 1
        try:
            compfunc = getattr(self, 'complete_arg_%d' % i)
            res = compfunc(text, line, begidx, endidx)
        except AttributeError:
            res = []
        except Exception as e:
            print('DEBUG: COMPLETE BUG', e)
            res = []
            
        return res

    def check_arg(self, arg):
        try:
            return self.argparser.parse_args(arg.split())
        except:
            raise SkyeyeAPIException([ERROR_ALL_F, ''])

    def __call__(self, *args, **kwargs):
        # CHECK
        ## 1.check state 
        if self.need_check_state:
            state_machine.check(self.name)
        ## 2.check arg
        if self.need_check_arg:
            kwargs['arg_ns'] = self.check_arg(kwargs['arg'])

        try:
            res = self.call(*args, **kwargs)
        except Exception as e:
            raise e
        
        if self.need_check_state:
            state_machine.transition(self.name)

# util func
def convert_int(x):
    return int(x, 0)
convert_int.__name__ = 'int'

def table_print(l):
    COL_NUM = 5
    ITEM_WIDTH = 20
    for i in range(0, len(l), COL_NUM):
        print(' '.join([item.ljust(ITEM_WIDTH) for item in l[i:i+COL_NUM]]))
