from . import SkyeyeCommand, convert_int, table_print
import argparse
import os
from skyeye_common_module import SkyEyeGetClassList, SkyEyeGetClassConnectList
from exception import SkyeyeAPIException, ERROR_ALL_F

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='list_connect',
                description='Show the connect information of a class.',
                add_help=False)

        parser.add_argument(
                'cls', 
                metavar='<class-name>',
                help='class name',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        cls_l = SkyEyeGetClassList()
        if arg_ns.cls not in cls_l:
            msg = 'information CONNECT "%s" was not found' % arg_ns.cls
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        connect_l = SkyEyeGetClassConnectList(arg_ns.cls)
        connect_l.sort()
        table_print(connect_l)

        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        cls_l = SkyEyeGetClassList()
        return [item for item in cls_l if item.startswith(text)]
