from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
import fault_inject as sfi
import pytimer
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='set_pc',
                description='set_pc',
                add_help=False)

        parser.add_argument(
                'cpu', 
                metavar='<cpu-name>',
                help='cpu name',
                )

        parser.add_argument(
                'value', 
                metavar='<value>',
                type=convert_int,
                help='value',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        ret = SkyEyeSetPC(arg_ns.cpu, arg_ns.value)

        if ret == 0:
            msg = 'SkyEyeSetPC Failed!'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        config = conf2.get_config()
        if not config:
            return []
        cpus = config.get_cpus()
        return [item for item in cpus if item.startswith(text)]

    def complete_arg_2(self, text, line, begidx, endidx):
        if '0x'.startswith(text):
            return ['0x']
        return []
