from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
import tools
import os
import sys
from conf import *
import traceback
import shutil
from se_path import InstallDir
from exception import *
import traceback
import shutil
from se_path import InstallDir
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='reverse_enable',
                description='reverse_enable',
                add_help=False)

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        config = conf2.get_config()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        ckpt = None
        if tools.is_windows():
            ckpt = os.path.join(InstallDir, ".reverse.ckpt")
        elif tools.is_linux():
            usr = os.path.expanduser('~')
            ckpt = os.path.join(usr, ".skyeye", "revere.ckpt")

        if GetReverseState():
            raise SkyeyeAPIException(['0x40099001'])

        SetReverseState(True)
        if os.path.exists(ckpt):
            shutil.rmtree(ckpt)
        full_ckpt_dir = tools.create_chp_dir(ckpt)

        mach_list = config.get_machines()
        for mach in mach_list:
            cpu = config.get_cpuname_by_mach(mach)
            device_list = config.get_devices(mach)
            for device in device_list:
                cls = config.get_classname(device)
                if cls == "image":
                    count = SkyEyeImageGetPageCount(device)
                    for i in range(0, count):
                        index =  SkyEyeImageGetPageIndexById(device, i)
                        image_file_name  = ''.join([device, "_%d" % index, ".image"])
                        image_file_path = os.path.join(full_ckpt_dir, image_file_name)
                        SkyEyeImagePageToFile(device, index, image_file_path)
                        tools.ckpt_save_image_info(full_ckpt_dir, device, index, image_file_name)
                steps = SkyEyeGetCpuSteps(cpu)
            tools.ckpt_save_steps(full_ckpt_dir, cpu, steps)
        config = os.path.join(full_ckpt_dir, "config")
        SkyEyeStoreConfigure(config)

        return False
