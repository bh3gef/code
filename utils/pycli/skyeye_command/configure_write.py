from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
from conf import GetGlobalConfig
import fault_inject as sfi
from conf import *
import tools

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='configure_write',
                description='configure_write',
                add_help=False)

        parser.add_argument(
                'snapshot', 
                metavar='<vmware-snapshot>',
                help='vmware snapshot',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        config = GetGlobalConfig()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        mach_list = config.get_mach_list()
        cmd_dir = arg_ns.snapshot

        json_file = config.get_current_fp()
        skyeye_script  = GetGlobalScript()
        files = [json_file, skyeye_script]
        if cmd_dir == None:
            cmd_dir = 'skyeye.ckpt'
        else:
            if cmd_dir.endswith(".ckpt") == False:
                cmd_dir = ''.join([cmd_dir, ".ckpt"])
        if tools.is_linux():
            ckpt_dir = os.path.join(os.getcwd(), cmd_dir)
        else:
            ws_dir = ws.get_workspace_dir()
            if not os.path.exists(ws_dir):
                raise SkyeyeAPIException(['0x401b1b01'])
            ckpt_dir = os.path.join(ws_dir, cmd_dir)
        try:
            full_dir = tools.create_chp_dir(ckpt_dir)
            tools.ckpt_save_files(full_dir, files)
        except IOError as e:
            raise SkyeyeAPIException(['0x401b0000', e])
        for mach in mach_list:
            cpu = config.get_cpuname_by_mach(mach)
            cls = config.get_classname(cpu)
            if cls == "x86":
                SkyEyeX86SaveConfigure(cpu, full_dir)
            device_list = config.get_device_list_by_mach(mach)
            for device in device_list:
                cls = config.get_classname(device)
                if cls == "image":
                    count = SkyEyeImageGetPageCount(device)
                    for i in range(0, count):
                        index =  SkyEyeImageGetPageIndexById(device, i)
                        image_file_name  = ''.join([device, "_%d" % index, ".image"])
                        image_file_path = os.path.join(full_dir, image_file_name)
                        SkyEyeImagePageToFile(device, index, image_file_path)
                        tools.ckpt_save_image_info(full_dir, device, index, image_file_name)
            steps = SkyEyeGetCpuSteps(cpu)
            tools.ckpt_save_steps(full_dir, cpu, steps)
        config = os.path.join(full_dir, "config")
        print(config)
        SkyEyeStoreConfigure(config)

        return False
