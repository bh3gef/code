from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import SkyEyeCreateBreakpoint
from exception import SkyeyeAPIException, ERROR_ALL_F
import fault_inject as sfi
import conf2

from skyeye_common_module import *

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='list_register',
                description='Show all registers of a device.',
                add_help=False)

        parser.add_argument(
                'device', 
                metavar='<device-name>',
                help='device name',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        config = conf2.get_config()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        device = config.get_device(arg_ns.device)
        if device is None:
            msg = 'No device "%s" in the current config.' % arg_ns.device
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        regs = device.get_register_list()
        if regs is None:
            msg = 'No registers on the the device "%s".' % arg_ns.device
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        regs.show()
        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        config = conf2.get_config()
        if not config:
            return []

        devices = config.get_devices()
        return [item for item in devices if item.startswith(text)]
