from . import SkyeyeCommand, convert_int
import argparse
import os
from exception import SkyeyeAPIException, ERROR_ALL_F
import fault_inject as sfi
from skyeye_common_module import *
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='set_register',
                description='Set a value  to a register.',
                add_help=False)

        parser.add_argument(
                'device', 
                metavar='<device-name>',
                help='device name',
                )

        parser.add_argument(
                'register', 
                metavar='<register-name>',
                help='register name',
                )

        parser.add_argument(
                'value', 
                metavar='<int value>',
                type=convert_int,
                help='value of int',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        regid = SkyEyeGetDevRegIdByName(None, arg_ns.device, arg_ns.register)
        ret = SkyEyeSetDevRegValueById(None, arg_ns.device, arg_ns.value, regid)
        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        config = conf2.get_config()
        if not config:
            return []
        devices = config.get_devices()
        return [item for item in devices if item.startswith(text)]

    def complete_arg_2(self, text, line, begidx, endidx):
        _, device_name, *_ = line.split()
        config = conf2.get_config()
        device = config.get_device(device_name)
        if not device:
            return []

        reg_list = device.get_register_list()
        if not reg_list:
            return []

        regs = reg_list.get_registers()
        return [item for item in regs if item.startswith(text)]

    def complete_arg_3(self, text, line, begidx, endidx):
        if '0x'.startswith(text):
            return ['0x']
        return []
