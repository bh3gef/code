from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
from conf import GetGlobalConfig
import tools
import os
import sys
import traceback
import shutil
from se_path import InstallDir
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='reverse',
                description='reverse',
                add_help=False)

        parser.add_argument(
                'cpu', 
                metavar='<cpu-name>',
                help='cpu name',
                )

        parser.add_argument(
                'size', 
                metavar='<step-size>',
                type=convert_int,
                help='step size',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        config = conf2.get_config()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        ckpt = None
        if tools.is_windows():
            ckpt = os.path.join(InstallDir, ".reverse.ckpt")
        elif tools.is_linux():
            usr = os.path.expanduser('~')
            ckpt = os.path.join(usr, ".skyeye", "revere.ckpt")

        t_cwd = os.getcwd()
        os.chdir(ckpt)
        if not GetReverseState():
            raise SkyeyeAPIException(['0x40131301'])

        core = args[0]
        if arg_ns.cpu not in config.get_cpu_list():
            raise SkyeyeAPIException(['0x40130004', arg_ns.cpu])

        reverse_steps = tools.str_to_uint(arg_ns.size)
        steps = tools.ckpt_get_steps(ckpt, core)
        current_steps = SkyEyeGetCpuSteps(core)
        run_steps = current_steps - steps - reverse_steps
        if current_steps == steps:
            return
        if reverse_steps > current_steps - steps:
            raise SkyeyeAPIException(['0x40131302', current_steps - steps])

        info_dic = tools.ckpt_get_image_info(ckpt)
        images = info_dic.keys()
        for image in images:
            SkyEyeImageClearAllPages(image)
            for page_l in info_dic[image]:
                SkyEyeImageFileToPage(image, page_l[0], page_l[1])
        config = os.path.join(ckpt, "config")
        SkyEyeRecoverConfigure(config)
        os.chdir(t_cwd)
        if run_steps != 0:
            SkyEyeStepi(core, "%d" % run_steps)

        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        config = conf2.get_config()
        if not config:
            return []
        cpus = config.get_cpus()
        return [item for item in cpus if item.startswith(text)]
