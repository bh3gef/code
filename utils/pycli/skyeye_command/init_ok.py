from . import SkyeyeCommand, convert_int
import argparse
import os
from exception import SkyeyeAPIException, ERROR_ALL_F
import se_system as ss
from skyeye_common_module import *

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='init_ok',
                description='init_ok',
                add_help=False)

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        SkyEyePrepareToRun()

        if ss.system is None:
            ss.CreateClass()

        return False
