from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import SkyEyeCreateBreakpoint
from exception import SkyeyeAPIException, ERROR_ALL_F
import conf2 

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='bp_create',
                description='Insert a break-point on a cpu-core.',
                add_help=False)
        
        parser.add_argument(
                'cpu', 
                metavar='<cpu-core>',
                help='cpu-core name',
                )

        parser.add_argument(
                'bp_addr', 
                metavar='<bp-addr>',
                type=convert_int,
                help='break-point address (int)',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        SkyEyeCreateBreakpoint(arg_ns.cpu, arg_ns.bp_addr)
        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        config = conf2.get_config()
        if not config:
            return []
        cpus = config.get_cpus()
        return [item for item in cpus if item.startswith(text)]

    def complete_arg_2(self, text, line, begidx, endidx):
        if '0x'.startswith(text):
            return ['0x']
        return []
