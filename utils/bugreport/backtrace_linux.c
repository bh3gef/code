#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <signal.h>                     /* for signal */
#include <execinfo.h>                   /* for backtrace() */

#include <skyeye_module.h>

#define BACKTRACE_SIZE   40

#define BUFFER_MAX (16*1024)

struct output_buffer
{
    char *buf;
    size_t sz;
    size_t ptr;
};

static char *g_output = NULL;

static void output_init(struct output_buffer *ob, char *buf, size_t sz)
{
    ob->buf = buf;
    ob->sz = sz;
    ob->ptr = 0;
    ob->buf[0] = '\0';
}

static void output_print(struct output_buffer *ob, const char *format, ...)
{
    if (ob->sz == ob->ptr)
        return;
    ob->buf[ob->ptr] = '\0';
    va_list ap;

    va_start(ap, format);
    vsnprintf(ob->buf + ob->ptr, ob->sz - ob->ptr, format, ap);
    va_end(ap);

    ob->ptr = strlen(ob->buf + ob->ptr) + ob->ptr;
}

static void dump(struct output_buffer *ob)
{
    int j, nptrs;
    void *buffer[BACKTRACE_SIZE];
    char **strings;

    nptrs = backtrace(buffer, BACKTRACE_SIZE);

    output_print(ob, "backtrace() returned %d addresses\n", nptrs);

    strings = backtrace_symbols(buffer, nptrs);
    if (strings == NULL)
    {
        perror("backtrace_symbols");
        exit(EXIT_FAILURE);
    }

    for (j = 0; j < nptrs; j++)
    {
        output_print(ob, "  [%02d] %s\n", j, strings[j]);
    }

    free(strings);
}

static void signal_handler(int signo, siginfo_t * info, void *secret)
{
    struct output_buffer ob;

    output_init(&ob, g_output, BUFFER_MAX);
#if 0
    char buff[64] = { 0x0 };

    sprintf(buff, "cat /proc/%d/maps", getpid());

    system((const char *) buff);
#endif

    output_print(&ob, "\n=========>>> catch signal %d <<<=========\n", signo);

    output_print(&ob, "Dump stack start...\n");
    dump(&ob);
    output_print(&ob, "Dump stack end...\n");

#define MAX_PATH 1024
    int l_i_tmp_len;
    char sysTempPath[MAX_PATH + 1];
    char tempFileName[MAX_PATH + 1];

    strncpy(sysTempPath, "/tmp/", MAX_PATH);
    l_i_tmp_len = strlen(sysTempPath);

    strncpy(sysTempPath + l_i_tmp_len, "skyeye_temp", MAX_PATH - l_i_tmp_len);

    // 创建测试结果目录
    if (access(sysTempPath, F_OK) != 0)
    {
        if (mkdir(sysTempPath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0)
        {
            fprintf(stderr, "Can not create skyeye_temp path: %s\n", sysTempPath);
            return 0;
        }
    }

    time_t now_time = time(0);
    struct tm *ltm = localtime(&now_time);
    char curr_time[64];

    sprintf(curr_time, "%d_%02d_%02d_%02d_%02d_%02d",
            1900 + ltm->tm_year, 1 + ltm->tm_mon, ltm->tm_mday, ltm->tm_hour, ltm->tm_min, ltm->tm_sec);
    sprintf(tempFileName, "%s/skyeye_bugreport_%s.log", sysTempPath, curr_time);

    fputs(g_output, stderr);
    FILE *output_logfile;

    output_logfile = fopen(tempFileName, "wb+");
    fputs(g_output, output_logfile);
    fflush(output_logfile);
    fclose(output_logfile);

    char curr_path[1024] = { 0 };
    readlink("/proc/self/exe", curr_path, 1024);

    int length = strrchr(curr_path, '/') + 1 - curr_path;

    curr_path[length] = '\0';

    sprintf(curr_path, "%sbugreport", curr_path);

    if (fork() == 0)
    {
        execl(curr_path, curr_path, "-f", tempFileName, "-t", curr_time, NULL);
    }

    signal(signo, SIG_DFL);             /* 恢复信号默认处理 */
    raise(signo);                       /* 重新发送信号 */
}

//__attribute__((dllexport)) void
void backtrace_register(void)
{
    if (g_output == NULL)
    {
        g_output = malloc(BUFFER_MAX);

        struct sigaction act;

        act.sa_sigaction = signal_handler;
        sigemptyset(&act.sa_mask);
        act.sa_flags = SA_RESTART | SA_SIGINFO;

        sigaction(SIGSEGV, &act, NULL);
        sigaction(SIGUSR1, &act, NULL);
        sigaction(SIGFPE, &act, NULL);
        sigaction(SIGILL, &act, NULL);
        sigaction(SIGBUS, &act, NULL);
        sigaction(SIGABRT, &act, NULL);
        sigaction(SIGSYS, &act, NULL);
    }
}

//__attribute__((dllexport)) void
void backtrace_unregister(void)
{
    if (g_output)
    {
        free(g_output);
    }
}

// SkyEye Module Load Method
#if 0
/*
 * the contructor for module. All the modules should implement it.
 */
void module_init() __attribute__ ((constructor));

/*
 * the decontructor for module. All the modules should implement it.
 */
void module_fini() __attribute__ ((destructor));

const char *skyeye_module = "backtrace_module";

void module_init()
{
    backtrace_register();
}

void module_fini()
{
    backtrace_unregister();
}
#endif
