#ifdef __WIN32__
#include <windows.h>
#endif

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <Python.h>

#ifdef __WIN32__
wchar_t* c2w(const char *str)
{
    int length = strlen(str) + 1;
    wchar_t *t = (wchar_t*)malloc(sizeof(wchar_t) * length);
    memset(t,0,length * sizeof(wchar_t));
    MultiByteToWideChar(CP_ACP,0,str,strlen(str),t,length);
    return t;
}
#endif

int main(int argc, char **argv)
{
    char opt = 0;
    char outfile[256] = { 0 };
    char timeStr[256] = { 0 };
    while ((opt = getopt(argc, argv, "f:t:h")) != -1)
    {
        switch (opt)
        {
            case 'f':
                strncpy(outfile, optarg, 256);
                break;
            case 't':
                strncpy(timeStr, optarg, 256);
                break;
            case 'h':
                printf("%s -f filename.log\n", argv[0]);
                return -1;
        }
    }

    if ((outfile[0] == 0) || (timeStr[0] == 0))
    {
        return -1;
    }

    char tmp_buffer[128] = { 0 };
    char skyeye_bin[128] = { 0 };
    char new_path[1024] = { 0 };

#ifdef __WIN32__
    GetModuleFileName(NULL, tmp_buffer, 1024);

    // Symbol conversion '\\' to '/'
    char *p;

    p = tmp_buffer;
    while (*p)
    {
        if (*p == '\\')
            *p = '/';
        p++;
    }

    int length = strrchr(tmp_buffer, '/') + 1 - tmp_buffer;

    strncpy(skyeye_bin, tmp_buffer, 128);
    skyeye_bin[length] = '\0';

#else
    sprintf(skyeye_bin, "/opt/skyeye/bin");
#endif

#ifdef __WIN32__
    char python_home[1024];

    sprintf(python_home, "%s%s", skyeye_bin, "/../../../lib/python35");
    Py_SetPythonHome(c2w(python_home));
#endif

    // Call Python Script
    Py_Initialize();

    PyRun_SimpleString("import sys\n");
    sprintf(new_path, "sys.path.append('%s')\n", skyeye_bin);
    PyRun_SimpleString(new_path);

    //PyRun_SimpleString("print sys.path\n");

    PyRun_SimpleString("from skyeye_bugreport_main import *\n");
    sprintf(new_path, "bugreport_main('%s', '%s')\n", outfile, timeStr);
    PyRun_SimpleString(new_path);

    Py_Finalize();

    return 0;
}
