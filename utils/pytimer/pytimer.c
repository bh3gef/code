#include "pytimer.h"
#include "skyeye_system.h"
#include "skyeye_mm.h"
#include "skyeye_obj.h"
#include "skyeye_log.h"
#include "memory_monitor.h"
#include "skyeye_api.h"

#ifdef __WIN32__
#include <windows.h>
#endif

static void PyTmrCallback(void *, time_data_t *);
static void PyPcWatchCallback(void *data);
static void PyMemoryWatchCallback(void *user_data);

/*
 ***********************************************************************************************
 *                              CREATE A TIMER FROM PYTHON
 * Destription:  This function is called from python to create timer callback in simulation
 *
 * Argument(s):  mode   Is mode of this timer
 * 
 * Return(s)  :  None
 *
 * Note(s)    :  None
 *
 ***********************************************************************************************
 */

void PythonTmrCreate(char *cpu_name, TM ms, PYTMR_MODE_T mode,
			py_callback_t py_cb_func, void *py_cb_data, 
			wait_func_t wait, notify_func_t notify)
{
    PYTMR *pytmr;
    conf_object_t *cpu = get_conf_obj(cpu_name);
    if (NULL == cpu)
    {
        skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpu_name);
        return 0;
    }

    pytmr = skyeye_mm_zero(sizeof (struct pytmr));
    pytmr->Mode = mode;
    pytmr->cpu = cpu;
    pytmr->py_cb_func = py_cb_func;
    pytmr->py_cb_data = py_cb_data;
    pytmr->wait = wait;
    pytmr->notify = notify;

    conf_object_t *idle_device = ((sys_cpu_t *) cpu->sys_struct)->sys_soc->idle_device->dev;

    uint32_t flags = SCHED_MS;
    switch (mode)
    {
        case PYTMR_ONE_SHOT:
            flags |= SCHED_ONCE;
            break;
        case PYTMR_PERIOD:
            flags |= SCHED_NORMAL;
            break;
        default:
            return 0u;
    }
    time_handle_t handler = system_register_timer_handler_with_data(idle_device, pytmr, ms, PyTmrCallback, flags);

    /*waiting for notifying*/
    pytmr->wait();

    /*clean up*/
    if (mode == PYTMR_PERIOD)
    	system_del_timer_handler(handler);
}

void PythonTmrDelete(notify_func_t notify) 
{
    notify();
}

void PythonSetWatchOnPc(char *cpu_name, uint32_t pc_addr,
			py_callback_t py_cb_func, void *py_cb_data, 
			wait_func_t wait, notify_func_t notify)
{
    PYTMR *pytmr;
    conf_object_t *cpu = get_conf_obj(cpu_name);

    if (NULL == cpu)
    {
        skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpu_name);
        return 0u;
    }
    pytmr = skyeye_mm_zero(sizeof (struct pytmr));
    pytmr->cpu = cpu;
    pytmr->py_cb_func = py_cb_func;
    pytmr->py_cb_data = py_cb_data;
    pytmr->wait = wait;
    pytmr->notify = notify;

    SkyEye_SetWatchOnPC(cpu_name, pc_addr, PyPcWatchCallback, pytmr);

    /*waiting for notifying*/
    pytmr->wait();

    /*clean up*/
    SkyEye_UnWatchOnPc(cpu_name, pc_addr);
}

void PythonUnWatchOnPc(notify_func_t notify)
{
    notify();
}

void PythonSetWatchonMemory(char *ms_name, int mm_type, uint32_t m_addr, 
				uint32_t data_type, uint32_t m_length,
				py_callback_t py_cb_func, void *py_cb_data, 
				wait_func_t wait, notify_func_t notify)
{
    PYTMR *pytmr;
    conf_object_t *mm_obj = get_conf_obj(ms_name);

    if (NULL == mm_obj)
    {
        skyeye_log(Error_log, __FUNCTION__, "Can not get mm_obj for %s.\n", ms_name);
        return 0;
    }
    pytmr = skyeye_mm_zero(sizeof (struct pytmr));
    pytmr->mm_space = mm_obj;
    pytmr->py_cb_func = py_cb_func;
    pytmr->py_cb_data = py_cb_data;
    pytmr->wait = wait;
    pytmr->notify = notify;

    /*Enable dyncom memory watch */
    SkyEye_SetDyncomWatchMemoryStatus(m_addr, 1);

    Py_SetWatchOnMemory(mm_obj, ms_name, mm_type, m_addr, data_type, m_length, PyMemoryWatchCallback, pytmr);

    /*wait for notifying*/
    pytmr->wait();

    /*clean up*/
    SkyEye_UnWatchOnMemory(ms_name, mm_type, m_addr, m_length);
}

int PythonUnWatchOnMemory(notify_func_t notify)
{
    notify();
}

/*
 ***********************************************************************************************
 *                              SYSTEM TIME CALLBACK
 * Destription:  This function is called when the time reach
 *
 * Argument(s):  arg    Is idle_device object pointer
 *              data    Is timer data pointer
 * 
 * Return(s)  :  None
 *
 * Note(s)    :  None
 *
 ***********************************************************************************************
 */

void PyTmrCallback(void *arg, time_data_t * data)
{
    PYTMR *pytmr = (PYTMR *) (data->user_data);
    pytmr->py_cb_func(pytmr->py_cb_data);
    if (pytmr->Mode == PYTMR_ONE_SHOT) 
    	pytmr->notify();
}

void PyPcWatchCallback(void *data)
{
    PYTMR *pytmr = (PYTMR *) (data);
    pytmr->py_cb_func(pytmr->py_cb_data);
}

void PyMemoryWatchCallback(void *data)
{
    PYTMR *pytmr = (PYTMR *) (data);
    pytmr->py_cb_func(pytmr->py_cb_data);
}
