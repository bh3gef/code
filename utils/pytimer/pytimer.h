#ifndef __PYTIMER__
#define __PYTIMER__
#include "skyeye_types.h"
#include "skyeye_lock.h"
#include "skyeye_system.h"
#include "mm_monitor.h"

typedef struct pytmr PYTMR;
typedef uint32_t PYTMR_ID;
typedef uint32_t TM;
typedef void (*py_callback_t) (void *data);
typedef void (*wait_func_t) ();
typedef void (*notify_func_t) ();

typedef enum
{
    PYTMR_ONE_SHOT,
    PYTMR_PERIOD,
} PYTMR_MODE_T;

typedef struct pytmr
{
    PYTMR_MODE_T Mode;
    PYTMR *NextPtr;
    PYTMR *PrevPtr;

    conf_object_t *cpu;
    conf_object_t *mm_space;

    py_callback_t py_cb_func;
    void *py_cb_data;
    wait_func_t wait;
    notify_func_t notify;
};
#endif
