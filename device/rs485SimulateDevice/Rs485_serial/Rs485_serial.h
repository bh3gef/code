/* Copyright (C)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
* 
*/
/**
* @file Rs485_serial.h
* @brief The definition of system controller 
* @author 
* @version 78.77
*/

/* Autogenerated by SkyEye script */
#ifndef __Rs485_serial_H__
#define __Rs485_serial_H__

typedef struct reg_send_cache{
	uint32_t send_empty :1;
	uint32_t reserved :31;
}send_cache_t;
typedef struct reg_recv_cache{
	uint32_t recv_full :1;
	uint32_t reserved :31;
}recv_cache_t;
typedef struct reg_cmd{
	uint32_t RE:1;
	uint32_t DE:1;
	uint32_t reserved :31;
}cmd_t;
typedef struct reg_status{
	uint32_t recv_finish:1;
	uint32_t send_finish:1;
	uint32_t reserved :31;
}stu_t;

#define FIFO_SIZE	1024
typedef struct Rs485_serial_reg{
	union {
		uint32_t value;
		send_cache_t flag;
	}send_cache;
	uint32_t send_data_length;
	union {
		uint32_t value;
		recv_cache_t flag;
	}recv_cache;
	uint32_t recv_data_length;
    uint32_t brsr_reg;   //波特率选择寄存器
	union{
		uint32_t value;
		cmd_t flag;
	}cmd_reg;
	union{
		uint32_t value;
		stu_t flag;
	}stu_reg;
	uint32_t addr_reg;
	//du add
	uint32_t ucr_reg;
	uint32_t mcr_reg;
	uint32_t icr_reg;
	uint32_t estu_reg;
	uint32_t count0_reg;
	uint32_t count1_reg;
	uint32_t count2_reg;
	uint32_t count3_reg;


}Rs485_serial_reg_t;

typedef struct Rs485_serial_device{
	conf_object_t* obj;
	Rs485_serial_reg_t* regs;
	struct fifo{
		char send_buf[FIFO_SIZE];
		char recv_buf[FIFO_SIZE];
		uint32_t start_send;
		uint32_t end_send;
		uint32_t start_recv;
		uint32_t end_recv;
	}fifo;
	int dev_id;
	Rs485_bus_intf *bus_iface;
	conf_object_t *bus_obj;
	char *send_buffer;
	char *recv_buf;
	int recv_cnt;
}Rs485_serial_device;

static char* regs_name[] = {
	"send_cache",
	"send_data_length", 
	"recv_cache",
	"recv_data_length", 
	"baud_rate_reg",
	"cmd_reg",
	"stu_reg",
	"id_addr_reg"
	"ucr_reg",
	"mcr_reg",
	"icr_reg",
	"estu_reg",
	"count0_reg",
	"count1_reg",
	"count2_reg",
	"count3_reg",
	NULL
};
#define OK      0
#define Error  -1
#endif
