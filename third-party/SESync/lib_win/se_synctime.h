#ifndef _SKYEYE_ENET_H
#define _SKYEYE_ENET_H

#include "se_synctime_types.h"

#define SKYEYE_ENET_API __declspec(dllimport)

#ifdef __cplusplus
extern "C"
{
#endif

/*========================= SubEngine_Register ===================================
 * 功能:
 *  注册模型信息。
 *
 * 参数:
 *  dStartTime: 模型起始时间，一般为0；
 *	dStopTime: 模型停止事件；
 *  dSyncStepSize: 协同仿真同步步长，dSyncStepSize<0时，SubEngine将根据各端口的采样速率计算同步步长
 *  emTrgMode: 触发模式；
 *  szPath: 模型路径；
 *  szModelName: 模型名称；
 *  szSoftware: 建模语言；
 *  szVersion: 建模语言；
 *  szModelVersion: 模型版本；
 *
 * 返回值:
 *  无。
 ===================================================================================*/

SKYEYE_ENET_API se_int32_t SubEngine_Register(char *client_name, char *server_ip, se_uint32_t server_port, se_uint32_t connect_max);

/*============================ SubEngine_Initialize ====================================
 * 功能:
 *  初始化子引擎。
 *
 * 参数:
 *  无
 *
 * 返回值:
 *  0:  成功
 *  -1: 失败
 *  -100: 没有授权信息
===================================================================================*/
SKYEYE_ENET_API se_int32_t SubEngine_Initialize();

/*============================ SubEngine_WaitForSyncClock ==========================
 * 功能:
 *  等待同步时钟。
 *
 * 参数:
 *  dCurTime: 当前的仿真时间(s)；
 *  ulMilliseconds: 等待超时时间(0~INFINITE）
 *
 * 返回值:
 *  SUBENG_NO_SYNC_CLOCK: 
 *  SUBENG_CONTINUE_SIMU: 
 *  SUBENG_SYNC_POINT: 
===================================================================================*/
#define SUBENG_NOT_IN_COSIM		0	/* 当前不是协同模式 */
#define SUBENG_CONTINUE_SIMU	1	/* 前序调用接收到过本步长的同步时钟 */
#define SUBENG_SYNC_POINT		2	/* 本次调用接收到本步长的同步时钟 */
#define SUBENG_NEED_SYNC_CLOCK	3	/* 尚未收到本步长的同步时钟，需要继续等待
									（当ulMilliseconds不为INFINITE时会返回本值 */
//SKYEYE_ENET_API hw_int32 SubEngine_WaitForSyncClock(double dCurTime, hw_uint32 ulMilliseconds/*=INFINITE*/);
SKYEYE_ENET_API se_int32_t SubEngine_WaitForSyncClock(double dCurTime);


/*============================ SubEngine_AddPort ====================================
 * 功能:
 *  添加端口。
 *
 * 参数:
 *  szPortName: 端口名称；
 *  emTrgMode: 触发模式；
 *  emDataType: 数据类型；
 *  ulDimension: 数据维度；
 *  lpInitData: 初始值，如果传入NULL,内部用0初始化缓冲区；
 *  usDepth: 端口队列深度；
 *  dSampleTime: 端口采样时间；
 * 返回值:
 *  端口索引，输入端口和输出端口的索引分别从0开始编号。
===================================================================================*/
SKYEYE_ENET_API se_int32_t SubEngine_AddPort(void *usr_data, const char* szPortName, Port_Call_Back_t call_back_func);

/*============================ SubEngine_WriteData ================================
 * 功能:
 *  写入输出端口数据队列。
 *
 * 参数:
 *  ulPortIdx: 端口索引；
 *  llTimestamp: 数据的时戳；
 *  lpData: 数据缓冲区地址；
 *  ulDataSize: 数据字节数
 * 返回值:
 *  写入的字节数: 
===================================================================================*/
SKYEYE_ENET_API se_int32_t SubEngine_WriteData(se_int32_t ulPortIdx, const void* lpData, se_uint32_t ulDataSize);

/*============================ SubEngine_Release ====================================
 * 功能:
 *  关闭子引擎。
 *
 * 参数:
 *  无
 *
 * 返回值:
 *  无。
===================================================================================*/
SKYEYE_ENET_API se_int32_t SubEngine_Release();

#ifdef __cplusplus
}
#endif

#endif
