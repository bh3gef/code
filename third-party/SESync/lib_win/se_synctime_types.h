#ifndef _SE_SYNCTIME_TYPES_H
#define _SE_SYNCTIME_TYPES_H

typedef enum _EM_HW_BASETYPE
{
	bt_double		= 0,	// 8 bytes
	bt_single,				// 4 bytes
	bt_char,				// 1 bytes
	bt_int8,				// 1 bytes
	bt_uint8,				// 1 bytes
	bt_int16,				// 2 bytes
	bt_uint16,				// 2 bytes
	bt_int32,				// 4 bytes
	bt_uint32,				// 4 bytes
	bt_int64,				// 8 bytes
	bt_uint64,				// 8 bytes
	bt_bool,				// 1 bytes
	bt_BOOL,				// 4 bytes
	bt_string,				// NULL terminated string

	bt_invalid				= 0xFF
} EM_HW_BASETYPE;

typedef signed char        se_int8_t;
typedef short              se_int16_t;
typedef int                se_int32_t;
typedef long long          se_int64_t;

typedef unsigned char      se_uint8_t;
typedef unsigned short     se_uint16_t;
typedef unsigned int       se_uint32_t;
typedef unsigned long long se_uint64_t;

typedef int (*Port_Call_Back_t)(void *usr_data, se_int32_t port_idx, void *data, se_uint32_t date_len);

typedef enum {
	Ret_OK,
	Ret_Error,
} enetret_t;

#endif
