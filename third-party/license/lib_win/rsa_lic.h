#ifndef _RSA_LIC_H
#define _RSA_LIC_H

#define SKYEYE_LIC_API __declspec(dllexport)

#ifdef __cplusplus
extern "C"
{
#endif

SKYEYE_LIC_API int licensecmp(char* filename);
SKYEYE_LIC_API char* get_cpu_list(void);
SKYEYE_LIC_API int get_allow_num(void);
SKYEYE_LIC_API const char* read_license_info(char* filename);
SKYEYE_LIC_API const char* get_cpuid_info(void);

#ifdef __cplusplus
}
#endif

#endif