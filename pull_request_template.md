ChangeLog:
===============================================
1. xxx
2. xxx
3. xxx


Proc:
===============================================
1. 使用【克隆/下载】里的命令，在 code 源码路径下，拉取 PR 分支，一般为 `git fetch https://gitee.com/open-skyeye/code.git pull/x/head:pr_x ` x 为 PR 序号。
2. 切换到对应分支 `git checkout pr_x`
3. 编译安装 Open-SkyEye
4. 使用 `git clone https://gitee.com/open-skyeye/testcase.git` 命令克隆测试用例 （如有需要）


Result:
===============================================
1. 预期结果


Tips:
===============================================
1. 创建 PR 后，请在 Gitee 右侧功能栏，指定代码审核人员和测试人员。
2. 如 PR 有关联 issues，请在右侧功能栏勾选关联的 issues